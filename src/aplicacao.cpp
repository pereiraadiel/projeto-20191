// Aplicação de jogo da velha pela equipe formada por:
//   Ana Clara Batista
//   Gabriela Lima
//   Iago Pinto
//   João Carneiro
//   João Gondim
//   Jürgen Fink
//
// O código consiste em funções procedurais, com variáveis locais e globais. As
// variáveis globais servem também como parâmetros de comunicação com outros
// arquivos do projeto, como a camada de aplicação e interface gráfica.
//
// A execução basicamente coleta dados do jogador local enquanto se comunica com
// ele via terminal, esperando a conexão de um adversário para iniciar a partida
// e coletar jogadas, modificando globalmente o tabuleiro.
// Nos momentos de espera da entrada do jogador remoto, o código verifica se a
// variável global relacionada à jogada dele foi alterada, e computa essa
// jogada. A cada jogada é conferido o fim da partida para então ser feito o
// anúncio do vencedor e aumentar a sua pontuação.
// Ao final de cada partida cada jogador decide se quer continuar jogando ou
// sair do jogo. Caso um jogador saia ou se desconecte o jogo acaba, as
// variáveis relacionadas são zeradas e as pontuações são anunciadas.


#include <camada-aplicacao.h>

#include <stdio.h>

#include <string>
#include <vector>
#include <random>
#include <iostream>

// Tamanho de buffer usado para strings
const unsigned int kTamanhoStr = 64;

// Variável de deploy pra mensagens de debugging
int debug = 1;

// Identificação do jogador, usado pela camada de aplicação
char nomeJogador[kTamanhoStr];

// Identificação do oponente, usado pela camada de aplicação
char nomeOponente[kTamanhoStr];

// Símbolos de cada jogador
char simboloJogador;
char simboloOponente;

// Indicador de jogador servidor, usado pela camada de aplicação
int isServer;

// Matriz 3x3 com as jogadas, utilizada pela interface grafica
char tabuleiro[3][3];

// Indicador de que jogador atual é o jogador 1
bool jogador1;

const char espacoVazio = '_';
const char jogador1Char = 'x', jogador2Char = 'o';
int contadorDeJogadas;
int contadorJogos;
int pontosJogador;
int pontosOponente;


// Coleta informações iniciais do jogador
void infoJogador();

// Inicia conexão com o outro jogador dependendo da escolha em ser servidor ou
// cliente. Faz uso das funções da camada de aplicação.
void iniciaConexao();

// Jogadores trocam nomes em sequência
void apresentacao();

// Sorteia qual jogador escolhe seu símbolo primeiro
void sorteiaPrimeiro();

// Pergunta ao jogador se ele quer repetir a operação
void tentaNovamente();

// Zera o contador de jogadas e preenche o tabuleiro com espaços vazios
void resetTabuleiro();

// Imprime tabuleiro atual no terminal nomeando coluna e linha
void printTabuleiro();

// Espera chegar jogada do oponente e computa no tabuleiro
void esperaOponente();

// Recebe a entrada do console e trata erros nos campos de linha e coluna
std::string recebeEntrada();

// Recebe jogada em int, confere validade e retorna falso para posição inválida
// ou espaço já opcupado e verdadeiro caso contrario, com mensagens
// correspondentes no terminal.
bool fazJogada(int lin, int col);

// Confere condições de vitoria ou derrota no tabuleiro em linhas, colunas
// ou diagonais, além de condições de empate por fim de jogadas restantes,
// e retorna verdadeiro caso o jogo tenha acabado e falso caso contrário,
// com mensagens correspondentes no terminal.
bool fimDeJogo();


int main() {
  int lin, col;
  pontosJogador = 0;
  pontosOponente = 0;
  contadorJogos = 1;

  std::cout << "\n\nNOVA SESSÃO\n\n";
  infoJogador();
  iniciaConexao();
  apresentacao();

  while (true) {
    std::cout << std::endl << std::endl << "COMEÇANDO JOGO " << contadorJogos
      << std::endl;
    sorteiaPrimeiro();
    resetTabuleiro();

    while (!fimDeJogo()) {
      if (jogador1 || contadorDeJogadas > 0) {
        printTabuleiro();
        std::cout << "Sua vez. Digite uma posição: ";
        char charLin, charCol;
        // Lê input de jogada até encontrar um válido
        do {
          std::string word = recebeEntrada();

          charLin = word[0];
          charCol = word[1];


          // Converte chars A, B, C em 0, 1, 2 respectivamente
          lin = static_cast<int>(charLin) - 65;
          // Converte chars 1, 2, 3 em 0, 1, 2 respectivamente
          col = static_cast<int>(charCol) - 49;
        } while (!fazJogada(lin, col));
      }

      if (fimDeJogo())
        break;
      else
        printTabuleiro();

      std::cout <<"Esperando oponente..." << std::endl << std::endl;
      esperaOponente();
    }
  }
  return 0;
}


void infoJogador() {
  bool falha;
  std::cout << "Insira seu nome: ";
  std::cin.getline(nomeJogador, kTamanhoStr);
  std::cout << "\nDeseja hospedar um servidor ou se conectar a um?\n"
    << "1. Cliente\n"
    << "2. Servidor\n";
  falha = true;
  do {
    std::cin >> isServer;
    isServer -= 1;
    if (isServer == 0 || isServer == 1) {
      falha = false;
    } else {
      std::cout << "\nEscolha inválida. Digite 1 ou 2:\n"
        << "1. Cliente\n"
        << "2. Servidor\n";
    }
  } while (falha);
}

// debug
// void iniciaConexao() {
//   char hostname[kTamanhoStr];
//   int porta;
//   bool falha;
//   if (isServer) {
//     std::cout << "\nDigite a porta em que o servidor vai operar: ";
//     // Tenta estabelecer conexão na porta fornecida e salvar hostname
//     falha = true;
//     do {
//       std::cin >> porta;
//       if ((criaServidor(porta)) != 0) {
//         std::cout << "\nTente novamente: ";
//       } else {
//         falha = false;
//       }
//     } while (falha);
//   } else {
//     // Tenta conectar com servidor de hostname e porta estabelecidos
//     falha = true;
//     do {
//       std::cout << "\nDigite o hostname do servidor: ";
//       std::cin >> hostname;
//       std::cout << "Digite a a porta: ";
//       std::cin >> porta;
//       if ((criaCliente(hostname, porta)) != 0) {
//         std::cout << "\nTente novamente:\n";
//       } else {
//         falha = false;
//       }
//     } while (falha);
//   }
// }

// debug
// void apresentacao() {
//   bool falha = true;
//   while (falha == true) {
//     if (isServer) {
//       if (enviaStr(nomeJogador) == 0) {
//         if (recebeStr(&nomeOponente) == 0) {
//           falha = false;
//           std::cout << "\n" << nomeOponente << " se conectou com sucesso!\n";
//         }
//       }
//     } else {
//       if (recebeStr(&nomeOponente) == 0) {
//         if (enviaStr(nomeJogador) == 0) {
//           falha = false;
//        std::cout << "\nSe conectou a " << nomeOponente << " com sucesso!\n";
//         }
//       }
//     }
//     if (falha == true) {
//       tentaNovamente();
//     }
//   }
// }

// debug
// void sorteiaPrimeiro() {
//   bool falha = true;
//   int primeiroJogador;
//   int simboloBin;
//   // Servidor gera numero aleatorio e comunica ao cliente
//   if (isServer) {
//     std::random_device gerador;
//     std::uniform_int_distribution<int> distribuicao(0, 1);
//     primeiroJogador = distribuicao(gerador);
//     while (falha == true) {
//       if (enviaInt(primeiroJogador) == 0) {
//         falha = false;
//       } else {
//         tentaNovamente();
//       }
//     }
//     falha = true;
//   } else {
//     while (falha == true) {
//       if (recebeInt(&primeiroJogador) == 0) {
//         falha = false;
//       } else {
//         tentaNovamente();
//       }
//     }
//     falha = true;
//   }
//   // Jogador sorteado escolhe o símbolo e comunica ao adversário
//   if (primeiroJogador == isServer) {
//     std::cout << "\nVocê foi sorteado para escolher seu símbolo.\n"
//       << "O jogador com " << jogador1Char << " começa a partida.\n"
//       << "\nQual símbolo você deseja? [x/o]\n";
//     do {
//       std::cin >> simboloJogador;
//       if (simboloJogador != jogador1Char && simboloJogador != jogador2Char) {
//         std::cout << "\nResponda com " << jogador1Char
//           << " ou " << jogador2Char << ": ";
//       }
//  } while (simboloJogador != jogador1Char && simboloJogador != jogador2Char);
//     if (simboloJogador == jogador1Char || simboloJogador == jogador1Char) {
//       simboloOponente = jogador2Char;
//       simboloBin = 1;
//     } else {
//       simboloOponente = jogador1Char;
//       simboloBin = 0;
//     }
//     while (falha == true) {
//    std::cout << "\nInformando a " << nomeOponente << " a escolha feita...\n";
//       if (enviaInt(simboloBin) == 0) {
//         falha = false;
//       } else {
//         tentaNovamente();
//       }
//     }
//   } else {
//     std::cout << std::endl << nomeOponente
//       << " foi sorteado para escolher o símbolo primeiro.\n"
//       << "Aguardando resposta...\n";
//     while (falha == true) {
//       if (recebeInt(&simboloBin) == 0) {
//         falha = false;
//         if (simboloBin == 0) {
//           simboloJogador = jogador1Char;
//           simboloOponente = jogador2Char;
//         } else {
//           simboloJogador = jogador2Char;
//           simboloOponente = jogador1Char;
//         }
//       } else {
//         tentaNovamente();
//       }
//     }
//   }
//   std::cout << "\nSímbolo de " << nomeJogador << ": " << simboloJogador
//     << std::endl
//     << "Símbolo de " << nomeOponente << ": " << simboloOponente << std::endl
//     << std::endl;
//   if (simboloJogador == jogador1Char) {
//     jogador1 = true;
//     if (DEBUG) std::cout << "jogador1 set true" << std::endl;
//   } else if (simboloJogador == jogador2Char) {
//     jogador1 = false;
//     if (DEBUG) std::cout << "jogador1 set false" << std::endl;
//   }
// }

void tentaNovamente() {
  char sn;
  std::cout << "\nNão foi possível trocar mensagens com o adversário.\n"
    << "Deseja tentar novamente? [s/n]\n";
  do {
    std::cin >> sn;
    if (sn != 's' && sn != 'S' && sn != 'n' && sn != 'N') {
      std::cout << "\nResponda com 's' ou 'n': ";
    }
  } while (sn != 's' && sn != 'S' && sn != 'n' && sn != 'N');
  if (sn == 'n' || sn == 'N') {
    std::cout << "\nEncerrando partida.\n";
    exit(EXIT_FAILURE);
  }
}

void resetTabuleiro() {
  contadorDeJogadas = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      tabuleiro[i][j] = espacoVazio;
}

void printTabuleiro() {
  std::cout << std::endl << "   ";
  for (int col = 1; col < 4; col++) {
    std::cout << col << " ";
  }
  std::cout << std::endl;
  for (int i = 0; i < 3; i++) {
    std::cout << char(65 + i) << "| ";
    for (int j = 0; j < 3; j++) {
      std::cout << tabuleiro[i][j] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

// debug
// void esperaOponente() {
//   int jogadaDoOponente;
//   while (recebeInt(&jogadaDoOponente) != 0) continue;

//   int lin = jogadaDoOponente / 3;
//   int col = jogadaDoOponente % 3;

//   tabuleiro[lin][col] = jogador1 ? jogador2Char : jogador1Char;
//   contadorDeJogadas++;
// }

bool fazJogada(int lin, int col) {
  if (lin < 0 || col < 0 || lin > 2 || col > 2) {
    std::cout << "\nPosição " << static_cast<char>(lin + 65) << col + 1
      << " inválida! Tente escrever uma linha seguida de uma coluna, "
      << "ex: \"B2\" ou \"A3\"." << std::endl;
    return false;
  }
  if (tabuleiro[lin][col] != espacoVazio) {
    std::cout << "\nEste campo já está ocupado! Tente outro campo: ";
    return false;
  }

// debug
//  int idCelula = 3 * lin + col;
//  while (enviaInt(idCelula) != 0) continue;
  tabuleiro[lin][col] = jogador1 ? jogador1Char : jogador2Char;
  contadorDeJogadas++;
  return true;
}

std::string recebeEntrada() {
  std::string word;
  std::cin >> word;
  while (word.length() != 2) {
    std::cout << "\nEntrada inválida! Tente escrever uma "
    <<"linha seguida de uma coluna, "
    << "ex: \"B2\" ou \"A3\"." << std::endl;
    std::cin >> word;
  }
  return word;
}

bool fimDeJogo() {
  bool acabouJogo = false;
  std::string porQueAcabou = "";
  for (int i = 0; i < 3; i++) {
    // lin
    if (tabuleiro[i][0] != espacoVazio &&
        tabuleiro[i][0] == tabuleiro[i][1] &&
        tabuleiro[i][1] == tabuleiro[i][2]) {
      if (tabuleiro[i][0] == jogador1Char && jogador1) {
        porQueAcabou = "VITORIA!";
        pontosJogador++;
      } else {
        porQueAcabou = "DERROTA!";
        pontosOponente++;
      }
      acabouJogo = true;
    }

    // col
    if (tabuleiro[0][i] != espacoVazio &&
        tabuleiro[0][i] == tabuleiro[1][i] &&
        tabuleiro[1][i] == tabuleiro[2][i]) {
      if ((tabuleiro[0][i] == jogador1Char && jogador1) ||
          (tabuleiro[0][i] == jogador2Char && !jogador1)) {
        porQueAcabou = "VITORIA!";
        pontosJogador++;
      } else {
        porQueAcabou = "DERROTA!";
        pontosOponente++;
      }
      acabouJogo = true;
    }
  }

  // diag
  if (tabuleiro[1][1] != espacoVazio) {
    if ((tabuleiro[0][0] == tabuleiro[1][1] &&
          tabuleiro[1][1] == tabuleiro[2][2]) ||
        (tabuleiro[0][2] == tabuleiro[1][1] &&
         tabuleiro[1][1] == tabuleiro[2][0])) {
      if ((tabuleiro[1][1] == jogador1Char && jogador1)
          || (tabuleiro[1][1] == jogador2Char && !jogador1)) {
        porQueAcabou = "VITORIA!";
        pontosJogador++;
      } else {
        porQueAcabou = "DERROTA!";
        pontosOponente++;
      }
      acabouJogo = true;
    }
  }

  // acabaram jogadas
  if (!acabouJogo) {
    bool acabouJogadas = true;
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        acabouJogadas = acabouJogadas && tabuleiro[i][j] != espacoVazio;

    if (acabouJogadas) {
      porQueAcabou = "EMPATE!";
      acabouJogo = true;
    }
  }

  if (acabouJogo) {
    char sn;
// debug
//    int revancheJogador;
//    int revancheOponente;
    contadorJogos++;
    printTabuleiro();
    std::cout << std::endl << std::endl
      << porQueAcabou << std::endl << std::endl;
    std::cout << " == PONTUAÇÃO == " << std::endl
      << nomeJogador << ": " << pontosJogador << "     "
      << nomeOponente << ": " << pontosOponente << std::endl << std::endl;
    std::cout << "Deseja jogar novamente? [s/n]\n";
    do {
      std::cin >> sn;
      if (sn != 's' && sn != 'S' && sn != 'n' && sn != 'N') {
        std::cout << "\nResponda com 's' ou 'n': ";
      }
    } while (sn != 's' && sn != 'S' && sn != 'n' && sn != 'N');

// debug
//    if (sn == 's' || sn == 'S') {
//      revancheJogador = 1;
//      while (enviaInt(revancheJogador) != 0) continue;
//      while (recebeInt(&revancheOponente) != 0) continue;
//      if (revancheOponente == 0) {
//        std::cout << std::endl<< nomeOponente << " rejeitou um novo jogo.\n"
//          << "\nEncerrando sessão.\n";
//        exit(EXIT_SUCCESS);
//      }
//    } else if (sn == 'n' || sn == 'N') {
//      revancheJogador = 0;
//      while (enviaInt(revancheJogador) != 0) continue;
//      std::cout << "\nEncerrando sessão.\n";
//      exit(EXIT_SUCCESS);
//    }
    return true;
  }
  return false;
}
