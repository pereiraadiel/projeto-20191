// Camada de aplicação do jogo da velha pela equipe:
//   Ana Clara Batista
//   Gabriela Lima
//   Iago Pinto
//   João Carneiro
//   João Gondim
//   Jürgen Fink
//
// Camada de Aplicação que implementa o protocolo Velha, que consiste na troca
// de strings de múltiplas linhas seguindo o formato com 4 linhas de cabeçalho:
//   Protocolo e versão
//   Cliente ou servidor
//   Pedido ou resposta
//   Tipo do pedido ou resposta
//   Dados (pode ter ou não, dependendo do tipo)
//
// Exemplo:
//   Velha 0.1
//   Servidor
//   Pedido
//   Nome
//   Joao


#include <camada-aplicacao.h>

#include <iostream>
#include <sstream>


// Constantes contendo os cabecalhos suportados pelo protocolo sao iniciadas no
// arquivo de header, na pasta ../include.
const int debug = 1;


// Transforma a linha em uma stream para ser lida pela funcao getline e dividida
// em substrings separadas por espaco, idealmente contendo o nome e numero de
// versao do protocolo respectivamente, tratando qualquer outro resultado como
// erro.
int confereProtocolo(const std::string linha) {
  std::stringstream sLinha(linha);
  std::vector<std::string> tokens;
  std::string token;
  std::string protocolo;
  std::string versao;
  float fVersao;

  // Salva no vetor todas strings separadas por espaço
  while (std::getline(sLinha, token, ' ')) {
    tokens.push_back(token);
  }
  protocolo = tokens.at(0);
  versao = tokens.at(1);
  fVersao = std::stof(versao);
  if (protocolo == nomeVelha) {
    if (fVersao <= versaoVelha) {
      return 0;
    } else {
      return -2;
    }
  } else {
    return -1;
  }
}


// Compara linha com strings que representam as origens da mensagem suportadas
// pelo protocolo.
int confereOrigem(const std::string linha) {
  if (linha == origensVelha.at(0)) {
    return 0;
  } else if (linha == origensVelha.at(1)) {
    return 1;
  } else {
    return -1;
  }
}

