# projeto-20191

Projeto para o trabalho de MATA59.

## Como compilar e rodar

``` bash
make
./bin/jogo-da-velha
```

## TODO

1. [ ] Aplicação: Jogo da velha em C++ procedural
    - [x] Fazer makefile
    - [x] Funcionamento básico
    - [x] Perguntar se o jogador quer ser servidor ou cliente
    - [x] Retornar IP e porta ao jogador servidor
    - [x] Trocar nomes entre jogadores
    - [x] Pedir IP e porta do servidor ao jogador cliente
    - [x] Trocar nomes entre jogadores
    - [x] Trocar jogadas
    - [x] Implementar decisão aleatória de qual jogador vai escolher seu símbolo
    primeiro
    - [x] Implementar nome
    - [ ] Hash de informação de conexão
    - [x] Implementar opção de continuar jogando
    - [x] Implementar pontuação
    - [ ] Interface gráfica (qt?)
    - [ ] Salvar histórico de jogadas em arquivo
2. [ ] Camada de aplicação
    - [ ] Checar cabeçalho de Velha
3. [ ] Demais camadas

## Links utilizados

- [Diretrizes google de estilo c++](https://google.github.io/styleguide/cppguide.html#C++_Version)
- [Guia de programação em rede (excelente)](http://beej.us/guide/bgnet/html/single/bgnet.html)
- [Tutorial de sockets](http://www.linuxhowtos.org/C_C++/socket.htm)
- [Escolhendo uma porta](https://stackoverflow.com/questions/218839/assigning-tcp-ip-ports-for-in-house-application-use)
- [Alternativas para encontrar um server na rede](https://stackoverflow.com/questions/34343108/how-do-i-get-the-host-name-of-a-server)
- [Mais alternativas para encontrar um server na rede](https://stackoverflow.com/questions/9482275/search-for-and-connect-to-a-local-server-c-programming)
