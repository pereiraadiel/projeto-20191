#ifndef INCLUDE_CAMADA_APLICACAO_H_
#define INCLUDE_CAMADA_APLICACAO_H_

#include <string>
#include <vector>

const char *nomeVelha = "Velha";
const float versaoVelha = 0.1;
const std::vector<std::string> origensVelha = {"Servidor", "Cliente"};
const std::vector<std::string> tiposVelha = {"Nome", "Jogada"};

// Valida a linha de protocolo no comeco da mensagem.
// Recebe a string da linha em questao.
// Retorna inteiros indicando sucesso ou erro:
//   0: Protocolo e versao compativeis;
//  -1: Identificacao de protocolo incompativel;
//  -2: Versao do protocolo Velha incompativel.
int confereProtocolo(const std::string linha);

// Valida linha de origem da mensagem.
// Recebe a string da linha em questao.
// Retorna inteiros indicando a origem ou erro:
//   0: Mensagem veio do servidor;
//   1: Mensagem veio do cliente;
//  -1: String de origem invalida.
int confereOrigem(const std::string linha);

#endif  // INCLUDE_CAMADA_APLICACAO_H_
