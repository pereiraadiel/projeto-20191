CC := g++
CFLAGS := -g -Wall -Wextra -Werror -std=c++11
INC := -I include

TARGET := jogo-da-velha

TARDIR := bin
SRCDIR := src
INCDIR := include
BUILDDIR := build

_SOURCES := aplicacao.cpp camada-aplicacao.cpp
SOURCES := $(patsubst %,$(SRCDIR)/%,$(_SOURCES))
_DEPS := camada-aplicacao.h
DEPS := $(patsubst %,$(INCDIR)/%,$(_DEPS))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.cpp=.o))

$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp $(DEPS)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(TARDIR)/$(TARGET): $(OBJECTS)
	@mkdir -p $(TARDIR)
	$(CC) -o $@ $^

clean:
	rm -rf $(BUILDDIR) $(TARDIR)

.PHONY: clean
